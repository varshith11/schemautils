public class SchemaUtils {
    public static String recordTypeId(String ObjectName,String RecordLabel)
    {
        return Schema.getGlobalDescribe().get(ObjectName).getDescribe().getRecordTypeInfosByName().get(RecordLabel).getRecordTypeId();
    }
}